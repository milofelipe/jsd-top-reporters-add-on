package com.milofelipe.jsd.topreporters;

import org.junit.Test;

import java.util.*;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class TopReportersServiceTest {
    private TopReportersService topReportersService = new TopReportersService();

    @Test
    public void testProcessTopReporters_ZeroReporters() {
        Map projectIssues = createProjectIssuesWithZeroRequests();
        List<Reporter> topReporters = topReportersService.processTopReporters(projectIssues);
        assertTrue(topReporters.isEmpty());
    }

    @Test
    public void testProcessTopReporters_OneReporterWithTwoRequests() {
        Map projectIssues = createProjectIssuesWithOneReporterWithTwoRequests();
        List<Reporter> topReporters = topReportersService.processTopReporters(projectIssues);
        assertEquals(1, topReporters.size());
        Reporter reporter = topReporters.get(0);
        assertEquals("Joe Strummer", reporter.getName());
        assertEquals("joe.strummer@theclash.com", reporter.getEmail());
        assertEquals(2, reporter.getRequestCount());
    }

    @Test
    public void testProcessTopReporters_ThreeReportersWithVaryingRequestsEach() {
        Map projectIssues = createProjectIssuesWithThreeReportersWithVaryingRequestsEach();
        List<Reporter> topReporters = topReportersService.processTopReporters(projectIssues);
        assertEquals(3, topReporters.size());
        Reporter reporter1 = topReporters.get(0);
        assertEquals("Joe Strummer", reporter1.getName());
        assertEquals("joe.strummer@theclash.com", reporter1.getEmail());
        assertEquals(4, reporter1.getRequestCount());

        Reporter reporter2 = topReporters.get(1);
        assertEquals("Mick Jones", reporter2.getName());
        assertEquals("mick.jones@theclash.com", reporter2.getEmail());
        assertEquals(3, reporter2.getRequestCount());

        Reporter reporter3 = topReporters.get(2);
        assertEquals("Paul Simonon", reporter3.getName());
        assertEquals("paul.simonon@theclash.com", reporter3.getEmail());
        assertEquals(1, reporter3.getRequestCount());
    }

    private Map createProjectIssuesWithZeroRequests() {
        Map projectIssues = new HashMap();
        projectIssues.put("issues", Collections.emptyList());
        return projectIssues;
    }

    private Map createProjectIssuesWithOneReporterWithTwoRequests() {
        Map projectIssues = new HashMap();
        List<Map> issues = new ArrayList<>();

        Map reporter = new HashMap();
        reporter.put("accountId", "1");
        reporter.put("name", "Joe Strummer");
        reporter.put("emailAddress", "joe.strummer@theclash.com");
        Map fields = new HashMap();
        fields.put("reporter", reporter);
        Map issue1 = new HashMap();
        issue1.put("fields", fields);
        issues.add(issue1);

        reporter = new HashMap();
        reporter.put("accountId", "1");
        reporter.put("name", "Joe Strummer");
        reporter.put("emailAddress", "joe.strummer@theclash.com");
        fields = new HashMap();
        fields.put("reporter", reporter);
        Map issue2 = new HashMap();
        issue2.put("fields", fields);
        issues.add(issue2);

        projectIssues.put("issues", issues);
        return projectIssues;
    }

    private Map createProjectIssuesWithThreeReportersWithVaryingRequestsEach() {
        Map projectIssues = new HashMap();
        List<Map> issues = new ArrayList<>();

        issues.addAll(createReporter2Issues());
        issues.addAll(createReporter3Issues());
        issues.addAll(createReporter1Issues());

        projectIssues.put("issues", issues);
        return projectIssues;
    }

    private List<Map> createReporter1Issues() {
        List<Map> issues = new ArrayList<>();

        Map reporter = new HashMap();
        reporter.put("accountId", "1");
        reporter.put("name", "Joe Strummer");
        reporter.put("emailAddress", "joe.strummer@theclash.com");
        Map fields = new HashMap();
        fields.put("reporter", reporter);
        Map issue1 = new HashMap();
        issue1.put("fields", fields);
        issues.add(issue1);

        reporter = new HashMap();
        reporter.put("accountId", "1");
        reporter.put("name", "Joe Strummer");
        reporter.put("emailAddress", "joe.strummer@theclash.com");
        fields = new HashMap();
        fields.put("reporter", reporter);
        Map issue2 = new HashMap();
        issue2.put("fields", fields);
        issues.add(issue2);

        reporter = new HashMap();
        reporter.put("accountId", "1");
        reporter.put("name", "Joe Strummer");
        reporter.put("emailAddress", "joe.strummer@theclash.com");
        fields = new HashMap();
        fields.put("reporter", reporter);
        Map issue3 = new HashMap();
        issue3.put("fields", fields);
        issues.add(issue3);

        reporter = new HashMap();
        reporter.put("accountId", "1");
        reporter.put("name", "Joe Strummer");
        reporter.put("emailAddress", "joe.strummer@theclash.com");
        fields = new HashMap();
        fields.put("reporter", reporter);
        Map issue4 = new HashMap();
        issue4.put("fields", fields);
        issues.add(issue4);

        return issues;
    }

    private List<Map> createReporter2Issues() {
        List<Map> issues = new ArrayList<>();

        Map reporter = new HashMap();
        reporter.put("accountId", "2");
        reporter.put("name", "Mick Jones");
        reporter.put("emailAddress", "mick.jones@theclash.com");
        Map fields = new HashMap();
        fields.put("reporter", reporter);
        Map issue1 = new HashMap();
        issue1.put("fields", fields);
        issues.add(issue1);

        reporter = new HashMap();
        reporter.put("accountId", "2");
        reporter.put("name", "Mick Jones");
        reporter.put("emailAddress", "mick.jones@theclash.com");
        fields = new HashMap();
        fields.put("reporter", reporter);
        Map issue2 = new HashMap();
        issue2.put("fields", fields);
        issues.add(issue2);

        reporter = new HashMap();
        reporter.put("accountId", "2");
        reporter.put("name", "Mick Jones");
        reporter.put("emailAddress", "mick.jones@theclash.com");
        fields = new HashMap();
        fields.put("reporter", reporter);
        Map issue3 = new HashMap();
        issue3.put("fields", fields);
        issues.add(issue3);

        return issues;
    }

    private List<Map> createReporter3Issues() {
        List<Map> issues = new ArrayList<>();

        Map reporter = new HashMap();
        reporter.put("accountId", "3");
        reporter.put("name", "Paul Simonon");
        reporter.put("emailAddress", "paul.simonon@theclash.com");
        Map fields = new HashMap();
        fields.put("reporter", reporter);
        Map issue1 = new HashMap();
        issue1.put("fields", fields);
        issues.add(issue1);

        return issues;
    }
}