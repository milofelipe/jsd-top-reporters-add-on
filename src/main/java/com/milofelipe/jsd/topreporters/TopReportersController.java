package com.milofelipe.jsd.topreporters;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.HashMap;
import java.util.List;

@Controller
public class TopReportersController {
    @Autowired
    private AtlassianHostRestClients atlassianHostRestClients;
    @Autowired
    private TopReportersService topReportersService;

    @GetMapping("/top-reporters/{projectId}")
    public String topReporters(@PathVariable String projectId, Model model) {

        // Only accurate if total project requests is less than or equal to 100. 100 seems to be the maximum number of results
        // returned by the API (even if value given is more than 100). Will not handle multiple queries to sum up total requests.
        HashMap projectIssues = atlassianHostRestClients.authenticatedAsAddon()
                .getForObject("/rest/api/3/search?jql=project=" + projectId + "&maxResults=100&fields=reporter", HashMap.class);

        List<Reporter> topReporters = topReportersService.processTopReporters(projectIssues);
        model.addAttribute("topReporters", topReporters);

        return "topreporters";
    }
}