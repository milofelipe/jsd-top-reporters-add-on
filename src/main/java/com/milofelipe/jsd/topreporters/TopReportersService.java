package com.milofelipe.jsd.topreporters;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TopReportersService {
    List<Reporter> processTopReporters(Map projectIssues) {
        Map<String, Reporter> reporters = new HashMap();

        List<Map> issues = (List<Map>) projectIssues.get("issues");
        for (Map issue : issues) {
            Map fields = (Map) issue.get("fields");
            Map reporterMap = (Map) fields.get("reporter");

            String accountId = (String) reporterMap.get("accountId");

            Reporter reporter = reporters.get(accountId);

            if (reporter == null) {
                String name = (String) reporterMap.get("name");
                String email = (String) reporterMap.get("emailAddress");
                reporter = new Reporter(accountId, name, email);
                reporters.put(accountId, reporter);
            }

            reporter.incrementRequestCount();
        }

        List<Reporter> topReporters = new ArrayList<>(reporters.values());
        topReporters.sort((o1, o2) -> Integer.compare(o2.getRequestCount(), o1.getRequestCount()));

        return topReporters;
    }
}