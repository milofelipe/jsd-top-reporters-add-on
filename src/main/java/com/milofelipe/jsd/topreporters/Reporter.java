package com.milofelipe.jsd.topreporters;

public class Reporter {
    private String accountId;
    private String name;
    private String email;
    private int requestCount;

    public Reporter(String accountId, String name, String email) {
        this.accountId = accountId;
        this.name = name;
        this.email = email;
    }

    public void incrementRequestCount() {
        requestCount++;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public int getRequestCount() {
        return requestCount;
    }
}