# Top Reporters JSD Add-on

Used [Spring Boot](http://projects.spring.io/spring-boot/) starter for building
Atlassian Connect add-ons for JIRA ([Software](https://developer.atlassian.com/cloud/jira/software/),
[Service Desk](https://developer.atlassian.com/cloud/jira/service-desk/) and [Core](https://developer.atlassian.com/cloud/jira/platform/))
and [Confluence](https://developer.atlassian.com/cloud/confluence/).

## Running the application

Build your project, then run the following command:

    mvn spring-boot:run
    
Your application should start up locally on port 8080. If you visit `http://localhost:8080/atlassian-connect.json` in your
browser, you should see your add-on descriptor.

### Referencing configuration values in the add-on descriptor

Placeholders for Spring configuration properties (`${...}`) can be used in both keys and values in the add-on descriptor
(`atlassian-connect.json`). This provides a convenient way to tie your add-on descriptor to your application configuration,
and is even more powerful when used in conjunction with Spring profiles. The placeholders are replaced with actual values
from configuration each time your add-on descriptor is requested.

`atlassian-connect-spring-boot-archetype` uses the configuration property `addon.base-url` to provide the value for the
`baseUrl` add-on descriptor element. If you have made your add-on accessible to the public internet using e.g.
[ngrok](https://ngrok.com/), you can use the following command to start your add-on with the correct base URL:

    mvn spring-boot:run -Drun.arguments="--addon.base-url=https://[my-subdomain].ngrok.io"

## TODOs

- Handle projects with more than 100 requests. Jira Rest API only returns a maximum of 100 records per call. Add-on implementation 
only does one API call for now.

- Remove Spring Data Jpa dependency if possible. Not used by the add-on.